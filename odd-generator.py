from sys import argv
b = int(argv[1])
max = 1 << b
al = int(max/8)
a = ", ".join(["170" for _ in range(0,al)])

print("#include <stdio.h>\n\
#include <stdint.h>\n\
#include <stdlib.h>")

print()

print(f'uint8_t lut[{al}] = {{ {a} }};')

print()

print('int main(int argc, char* argv[]){\n\
	uint32_t number = strtoul(argv[1], NULL, 10);\n\
	if(lut[number/8] & (1 << number % 8)){\n\
		printf("odd\\n");\n\
	}else{\n\
		printf("even\\n");\n\
	}\n\
}')
