# odd-odd

Ok, y'all know that "implementing an even/odd function by enumerating & hardcoding all the numbers" meme?
Someone decided to implement that for the entire unsigned 32bit integer space & the blog article is 100% worth a read: [Andreas Karlsson: 4 billion if statements](https://andreasjhkarlsson.github.io//jekyll/update/2023/12/27/4-billion-if-statements.html)

But it got me thinking: This is a very inefficient way to do this, leading to a large executable (multiple bytes per number), slow (for large inputs), and completely unsuited for applications where you care about preventing timing side channels. 

## The solution: Lookup Tables

But there is a much more efficient solution that tackles all of those problems: Look-up-tables. Essentially: We do precomputations (e.g. using Andreas' program or a mathematical proof) to generate a list of all input-output pairs and include them in our program, during execution we simply need to look up the output belonging to our input pair. 

As we only need to store a single bit per output we can compress 8 outputs into one byte, plus a constant overhead for the main function & other definitions. As such a program for all 32bit unsigned integers should be approximately 537MB + a small constant, a major improvement in size.

Similarly we should see a major improvement in the area of execution time & side effect reduction - assuming we can fit the entire program in RAM (something that should be manageable for 32bit given the previous estimate) we should see near-constant execution times due to the nature of LUTs. However I would not want to promise complete timing-side-channel freeness and would, as always, strongly recommend using an untested program like mine in any critical application or any cryptographic product.

## Implementation

The C program follows a very basic structure, here an example for 4-bit integers:

```C
// Include necessary libraries
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

// Declare & define the LUT.
uint8_t lut[2] = { 170, 170 };

// Main function, taking one argument from the command line
int main(int argc, char* argv[]){
	uint32_t number = strtoul(argv[1], NULL, 10);
    // Perform lookup for the appropriate octet in the LUT & extract the correct bit within the octet
	if(lut[number/8] & (1 << number % 8)){
		printf("odd\n");
	}else{
		printf("even\n");
	}
}
```

Here the following values depend on the intended bit size `b`: 
* The length of the `lut` (`2^b/8`)
* The number of repetitions of the `170` in the `lut` (again, `2^b/8`)

Due to the lack of LaTeX support by GitLab MD & my Mastodon instance I ommitted the mathematical proof for the correctness of using 170 as the repeating value in the LUT.

This then allows us to write the following python meta-program:
```python
from sys import argv
b = int(argv[1])
max = 1 << b
al = int(max/8)
a = ", ".join(["170" for _ in range(0,al)])

print("#include <stdio.h>\n\
#include <stdint.h>\n\
#include <stdlib.h>")

print()

print(f'uint8_t lut[{al}] = {{ {a} }};')

print()

print('int main(int argc, char* argv[]){\n\
	uint32_t number = strtoul(argv[1], NULL, 10);\n\
	if(lut[number/8] & (1 << number % 8)){\n\
		printf("odd\\n");\n\
	}else{\n\
		printf("even\\n");\n\
	}\n\
}')
```

## Using this program

`odd-generator.py` is a simple python script that will take a positive integer and will output C source code. I recommend using even integers (ironic, I know) >= 4. Also keep in mind that the output will grow asymptotically exponentially to the input. Usage example:
```bash
python3 odd-generator.py 16 | tee odd.c 
```

The resulting C code then calculates the odd-or-even'ness for numbers between 0 and 2^b-1. To avoid the compiler messing things up I recommend compiling with the `-O0` flag. Usage example:
```bash
gcc -O0 -o odd odd.c
```

This then gives you the final executable. Usage:
```bash
$ ./odd 17
odd
$ ./odd 18
even
```

## Caveats & Issues
While the file size is substantially smaller than Andreas' solution & execution time should be near-constant given a specific program there are still pratical limits. Filesizes (both `.c` and the final executable) grow roughly exponentially with the bit length (leading to a 2.6GB `.c` file and `// unknown, compilation not finished //` executable file for 32bits). 
In addition compilation can be rather demanding, requiring upwards of 32GB of memory using default gcc settings (except `-O0`). On the positive side: I feel slightly less bad about overspecing the family homelab.

This especially means that the code will not work for more obscure architectures relying on anything larger than roughly 32bit. This however shouldn't be a problem as we, as of writing this, have another 14 years to tackle any potential issues arising from that.

In addition the code, as currently written, does not perform upper/lower bounds checks, with inputs outside `0 <= input < 2^b` yielding potentially incorrect outputs or leading to crashes. This would be an easy fix but I am yet to have breakfast.

## Results

// TODO